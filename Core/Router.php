<?php

namespace Core;
use Core\RouterTrait;

class Router
{
    use RouterTrait;
    /**
     * URLs array
     * @var array
     */
    protected array $routes = [];
    /**
     * URL's parameters
     * @var array
     */
    protected array $parameters = [];

    protected string $namespace = "App\Controllers\\";

    protected array $convertTypes = [
        "d" => "int",
        "s" => "string"
    ];

    /**
     * Adding routes
     * @param string $route
     * @param array $parameters
     */
    public function addRoutes(string $route, array $parameters =[]) {
        $route = preg_replace('/\//', '\\/', $route);
        $route = preg_replace('/\{([a-z]+)\}/', '(?P<\1>[a-z-]+)', $route);
        $route = preg_replace('/\{([a-z]+):([^\}]+)\}/', '(?P<\1>\2)', $route);
        $route = '/^' . $route . '$/i';
        $this->routes[$route] = $parameters;
    }

    public function getRoutes(): array {
        return $this->routes;
    }

    public function getParameters(): array {
        return $this->parameters;
    }

    /**
     * Matching the routes and set $parameters in case of route existence
     * @param string $url
     * @return bool
     */
    public function matchRoutes(string $url): bool {
        //dd($this->routes);
        foreach ($this->routes as $route => $parameters) {
            if (preg_match($route, $url, $matches)) {
                preg_match_all("/\(\?P<[\w]+>\\\\([\w\+]+)\)/", $route, $types);

                $step = 0;
                foreach ($matches as $key => $match) {
                    if (is_string($key)) {
                        $type = trim($types[1][$step], "+");
                        settype($match, $this->convertTypes[$type]);
                        $parameters[$key] = $match;
                        $step++;
                    }
                }
                $this->parameters = $parameters;
                dd($this->parameters);
                return true;
            }

        }
        return false;
    }

    public function dispatch(string $url = "") {
        $url = $this->queryStringVarRemove($url);
        if ($this->matchRoutes($url)) {
            $controller = $this->parameters['controller'];
            $controller = $this->getNamespace() . $this->convertStudly($controller);

            if (class_exists($controller)) {
                $controllerInstance = new $controller($this->parameters);
                $action = $this->parameters['action'];
                $action = $this->convertToCamelCase($action);

                if (preg_match("/action$/i", $action) == 0) {
                    call_user_func([$controllerInstance, $action]);
                } else {
                    $msg = "Method {$action} in controller {$controller} can't be called" .
                        " - remove the action suffix to call this method";
                    throw new \Exception($msg);
                }
            } else {
                throw new \Exception("Controller class {$controller} doesn't exists");
            }
        } else {
            throw new \Exception("No routes matched", 404);
        }
    }
}
?>