<?php

namespace Core;

trait RouterTrait
{
    /**
     * @param $string
     * @return array/string/string[]
     *
     */
    protected function convertStudly(string $string) {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    }

    /**
     * @param $string
     * @return string
     */
    protected function convertToCamelCase(string $string): string {
        return lcfirst($this->convertStudly($string));
    }

    protected function queryStringVarRemove(string $url): string {
        if ($url != '') {
            $parts = explode('&', $url, 2);
            if (strpos($parts[0], '=') === false) {
                $url = $parts[0];
            } else {
                $url = '';
            }
        }
        return $url;
    }

    protected function  getNamespace(): string {
        $namespace = $this->namespace;
        if (array_key_exists('namespace', $this->parameters)) {
            $namespace .= $this->parameters['namespace'] . '\\';
        }
        return $namespace;
    }
}
?>
