<?php
namespace Core;

use Core\Router;

$router->addRoutes("", ['controller' => 'Home', 'action' => 'index']);
$router->addRoutes("users/{id:\d+}/show", ['controller' => 'Users', 'action' => 'index' ]);
$router->addRoutes("posts/{id:\d}/edit", ['controller' => 'PostsController', 'action' => 'edit']);
