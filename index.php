<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


require_once "vendor/autoload.php";

use Core\Router;

$router = new Router();

include_once "Core/routes.php";

try {
    $path = trim($_SERVER['REQUEST_URI'], "/");
    //dd($path);
    $router->dispatch($path);
} catch (Exception $error) {
    $error->getMessage();
}





